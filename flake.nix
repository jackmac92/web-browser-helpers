{
  description = "A Racket application with dynamic Raco package handling";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        # Function to read info from info.rkt
        readInfo = pkgs.writeText "read-info.rkt" ''
          #lang racket/base
          (require syntax/parse/define)
          (define-simple-macro (info-reader)
            (begin
              (define info-path (getenv "INFO_RKT_PATH"))
              (unless info-path
                (error "INFO_RKT_PATH environment variable not set"))
              (printf "Using info-path of: ~a" info-path)
              (require (file info-path))
              (printf "~a\n~a"
                      (hash-ref (get-info/full info-path) 'name "unnamed")
                      (hash-ref (get-info/full info-path) 'version "0.0.0"))))
          (info-reader)
        '';

        # Read info from info.rkt
        infoOutput = builtins.split "\n" (builtins.readFile
          (pkgs.runCommand "read-info" {
            buildInputs = [ pkgs.racket ];
            INFO_RKT_PATH = toString ./info.rkt;
          } ''
            ${pkgs.racket}/bin/racket ${readInfo} > $out
          ''));

        # appName = builtins.elemAt infoOutput 0;
        appName = "web-browser-helpers";
        # appVersion = builtins.elemAt infoOutput 1;
        appVersion = "0.8.0";

      in {
        packages.default = pkgs.stdenv.mkDerivation {
          pname = pkgs.lib.info "appName ${appName}" appName;
          version = pkgs.lib.info "appVersion ${appVersion}" appVersion;
          src = ./.;

          buildInputs = [ pkgs.racket pkgs.cacert ];

          # Allow network access during build
          __noChroot = true;

          buildPhase = ''
            export HOME=$TMPDIR
            export SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
            # Install packages using raco
            ${pkgs.racket}/bin/raco pkg install --no-docs --batch --auto
            # Create executable using raco exe
            ${pkgs.racket}/bin/raco exe -o ${appName} main.rkt
          '';

          installPhase = ''
            mkdir -p $out/bin
            cp ${appName} $out/bin/
          '';
        };

        # devShells.default = pkgs.mkShell {
        #   buildInputs = [ pkgs.racket pkgs.cacert ];
        #   shellHook = ''
        #     export HOME=$TMPDIR
        #     export SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
        #     ${pkgs.racket}/bin/raco pkg install --no-docs --batch --auto
        #   '';
        # };
      });
}
